import sys
#import main discord module
import discord
#discord library has a builtin support for commands
from discord.ext import commands
#import asyncio for asynchronous support
import asyncio
#to reload modules without continuously restarting bot
import imp
#config module for adding token and meta data
import config
import fun
import util
import events
import admin


#create instance of the discord bot client (main object) from commands module
kizuna = commands.Bot(command_prefix = "?", description = "Kizuna Ai")

#kizuna.event is called when an event or change is detected
@kizuna.event
#in this case the event is on_ready, meaning the bot is ready and connected
async def on_ready():
    '''
    Function definition for when client (kizuna) is ready and connected
    Prints the following to console: Username of bot, ID of bot
    '''
    print("HAI DOMO, logged in as")
    print(kizuna.user.name)
    print(kizuna.user.id)
    print("------")

#kizuna.command is called when a user message has a command
@kizuna.command(description = "Say hi!")
#name of function is name of command due to discord library's nature of parsing
async def hello():
    #use "await" keyword only if a function uses "async" keyword
    await kizuna.say("Hai domo")

#fun

@kizuna.command(pass_context = True, description = "Hug another user!")
async def hug(huger,*hugee):
    await fun.hug(discord, kizuna, huger, *hugee)

@kizuna.command(pass_context = True, description = "Touch someone! ( ͡° ͜ʖ ͡°)")
async def touch(toucher,*touchee):
    await fun.touch(discord, kizuna, toucher, *touchee)

@kizuna.command(description = "Random Kizuna Picture")
async def ai():
    await fun.ai(discord, kizuna)

# Admin commands

@kizuna.command(pass_context = True, description = "Set a welcome message!")
async def welcome(ctx, channel, *msg):
    await events.welcome(discord, kizuna, ctx, channel, *msg)

@kizuna.command(pass_context = True, description = "Disable a command")
async def disable(ctx, command):
    await admin.disable(discord, kizuna, ctx, command)

@kizuna.command(pass_context = True, description = "Enable a command")
async def enable(ctx, command):
    await admin.enable(discord, kizuna, ctx, command)

#utils

@kizuna.command(pass_context = True, description = "Goodbye Domo Kizuna Bot")
async def leave(ctx):
    await util.leave(ctx, sys, kizuna)

@kizuna.command(pass_context = True, description = "Evaluate snippets")
async def e(ctx, *args):
    await util.e(ctx, kizuna, *args)

@kizuna.command(pass_context = True, description = "Check bot uptime")
async def uptime(ctx):
    await util.uptime(ctx)

#reload module

@kizuna.command(pass_context = True, description = "reload module")
async def reload(ctx, *args):
    try:
        for i in args:
            imp.reload(eval(i))
        await kizuna.say("**Reloaded the following module(s)**: " + " | ".join(args))
    except Exception as e:
        await kizuna.say(str(e))

# Events
@kizuna.event
async def on_member_join(member):
    await events.welcomeMessage(discord, kizuna, member)

# initiate the bot with the token as a string
if config.bot_token== "":
    token = input("Input token: ")
    kizuna.run(token)
else:
    kizuna.run(config.bot_token)
