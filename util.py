import asyncio
import config
import time
import datetime
import helper

start = time.time()

async def leave(ctx, sys, kizuna):
    #Check if user is in admin list in config
    if ctx.message.author.id in config.admins:
        #leave message
        await kizuna.say("Peace out weebs.")
        #logout of session
        await kizuna.logout()
        #close websocket connections and whatnot
        await kizuna.close()
        #exit
        sys.exit()

async def e(ctx, kizuna, *args):
    if ctx.message.author.id in config.admins:
        #error handler
        try:
            start = time.clock()
            proc = " ".join(args)
            ret = eval(proc)
            proc_time = str(time.clock())
            await kizuna.say(str(ret) + " **『eval time: " + "{:.3e}".format(float(proc_time)) + " s』**")
        except Exception as e:
            await kizuna.say(str(e))

async def uptime(ctx):
    if ctx.message.author.id in config.admins:
        diff = time.time() - start
        display = helper.time_to_string(diff)
        await ctx.bot.say("Online for: " + display)
    
