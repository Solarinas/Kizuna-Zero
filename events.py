import database
import admin
from helper import channelExists

commands = {"welcome":["channelID", "msg"]}

event_db = database.events(commands)

async def welcome(discord, kizuna, ctx, channel, *msg):
    if ctx.message.author.server_permissions.administrator:
        channels = channelExists(discord, channel, ctx)
        if channels:
            if event_db.manager.entryExists(event_db, "welcome", ctx.message.server.id):
                event_db.updateWelcome("welcome", channel, msg, ctx.message.server.id)
            else:
                event_db.manager.addEntry(event_db, "welcome", ctx.message.server.id)
                event_db.updateWelcome("welcome", channel, msg, ctx.message.server.id)
            await kizuna.say("Welcome message updated!")
        else:
            await kizuna.say("You did not enetr a valid channel name")
    else:
        await kizuna.say("You do not have permission to do that")

async def welcomeMessage(discord, kizuna, member):
    if event_db.manager.entryExists(event_db, "welcome", member.server.id):
        try:
            if event_db.manager.returnAttribute(database.admin(admin.commands), "disable", "welcome", member.server.id) == False:
                return False
            else:
                pass
        except:
            pass
        message = event_db.manager.returnAttribute(event_db, "welcome", "msg", member.server.id)
        channel = event_db.manager.returnAttribute(event_db, "welcome", "channelID", member.server.id)
        location = discord.utils.get(member.server.channels, name = channel)
        await kizuna.send_message(location, message)
    return False
