import asyncio
import random
import helper
import access
import database

#dictionary of commands in this module that require a db with required attributes
commands = {"hug":["counter"], "touch":["counter", "std"]}

#start up database for this module
fun_db = database.fun(commands)

async def hug(discord, kizuna, huger, *hugee):
    if len(hugee) > 0: # Check if hugee has any arguements
        member = helper.getUsers(discord, huger.message.server.members, hugee, huger) # Grab user data on all users in the arguement
        users = helper.duplicateCleaner([i.mention for i in member]) # Convert the data into strings and remove all duplicates
        hugeeIDs = helper.duplicateCleaner([i.id for i in member])

        image = access.randomImage("hug")
        chosen = random.choice(image)

        if len(users) == 1: # Check if array only has one object
            if huger.message.author in member: # Check if the huger is in the array and display a special message for them hugging themselves
                fancy = discord.Embed(description = huger.message.author.display_name + " hugs themselves. What a fucking loser", color = 0xDEADBF)
                fancy.set_image(url = chosen)
                await kizuna.say(embed = fancy)
            else: # if the huger is not in the array then just do a normal hug
                if fun_db.manager.entryExists(fun_db, "hug", huger.message.author.id):
                    fun_db.updateCounter("hug", huger.message.author.id, hugeeIDs)
                else:
                    fun_db.manager.addEntry(fun_db, "hug", huger.message.author.id)
                    fun_db.updateCounter("hug", huger.message.author.id, hugeeIDs)
                counter = fun_db.getUserCount("hug", huger.message.author.id, hugeeIDs)
                amount = counter["<@"+hugeeIDs[0]+">"]
                fancy = discord.Embed(description = huger.message.author.display_name + " hugs " + " ".join(users) + ". They have hugged them " + str(amount) + " times.", color = 0x0DEADBF)
                fancy.set_image(url = chosen)
                await kizuna.say(embed = fancy)
        elif len(users) > 1: # Check if there is more then 1 object in the array
            if huger.message.author in member: # Check if the huger is in the array and remove them from the array
                users.remove(huger.message.author.mention)
                toucheeIDs.remove(huger.message.author.id)
            if fun_db.manager.entryExists(fun_db, "hug", huger.message.author.id):
                #update user counter
                fun_db.updateCounter("hug", huger.message.author.id, hugeeIDs)
            else:
                #create user entry then update user counter
                fun_db.manager.addEntry(fun_db, "hug", huger.message.author.id)
                fun_db.updateCounter("hug", huger.message.author.id, hugeeIDs)
            counter = fun_db.getUserCount("hug", huger.message.author.id, hugeeIDs)
            string = []
            for i in counter:
                string.append(i + "**(" + str(counter[i]) + ")**")
            fancy = discord.Embed(description = huger.message.author.display_name + " group hugs " + ", ".join(string[0:len(users)-1]) + " and " + string[len(users)-1], color = 0xDEADBF) # Join the array together into a string for the group hug
            fancy.set_image(url = chosen)
            await kizuna.say(embed = fancy)
        else: # Display a special message if the array is empty
            await kizuna.say("Enter a valid username you dumbass")
    else: # Display a special message of hugee has no arguements
        await kizuna.say("You hugged no one dumbass")

async def touch(discord, kizuna, toucher, *touchee):
    if len(touchee) > 0: # Check if touchee has any arguements
        member = helper.getUsers(discord, toucher.message.server.members, touchee, toucher) # Grab user data on all users in the arguement
        users = helper.duplicateCleaner([i.mention for i in member]) # Convert the data into strings and remove all duplicates
        toucheeIDs = helper.duplicateCleaner([i.id for i in member])

        image = access.randomImage("touch")
        chosen = random.choice(image)

        if len(users) == 1: # Check if array only has one object
            if toucher.message.author in member: # Check if the toucher is in the array and display a special message for them touching themselves
                fancy = discord.Embed(description = toucher.message.author.display_name + " touches themselves. Bleh!", color = 0xDEADBF)
                fancy.set_image(url = chosen)
                await kizuna.say(embed = fancy)
            else: # if the toucher is not in the array then just do a normal touch
                #if user in in database
                if fun_db.manager.entryExists(fun_db,"touch", toucher.message.author.id):
                    #update user counter
                    fun_db.updateCounter("touch", toucher.message.author.id, toucheeIDs)
                else:
                    #create user entry then update user counter
                    fun_db.manager.addEntry(fun_db, "touch", toucher.message.author.id)
                    fun_db.updateCounter("touch", toucher.message.author.id, toucheeIDs)
                #get how many times a certain user has been touched
                counter = fun_db.getUserCount("touch", toucher.message.author.id, toucheeIDs)
                amount = counter["<@"+toucheeIDs[0]+">"]
                fancy = discord.Embed(description = toucher.message.author.display_name + " touches " + " ".join(users) + ". They have touched them " + str(amount) + " times.", color = 0x0DEADBF)
                fancy.set_image(url = chosen)
                await kizuna.say(embed = fancy)
        elif len(users) > 1: # Check if there is more then 1 object in the array
            if toucher.message.author in member: # Check if the toucher is in the array and remove them from the array
                users.remove(toucher.message.author.mention)
                toucheeIDs.remove(toucher.message.author.id)
            if fun_db.manager.entryExists(fun_db, "touch", toucher.message.author.id):
                #update user counter
                fun_db.updateCounter("touch", toucher.message.author.id, toucheeIDs)
            else:
                #create user entry then update user counter
                fun_db.manager.addEntry(fun_db, "touch", toucher.message.author.id)
                fun_db.updateCounter("touch", toucher.message.author.id, toucheeIDs)
            counter = fun_db.getUserCount("touch", toucher.message.author.id, toucheeIDs)
            string = []
            for i in counter:
                string.append(i + "**(" + str(counter[i]) + ")**")
            fancy = discord.Embed(description = toucher.message.author.display_name + " touches " + ", ".join(string[0:len(users)-1]) + " and " + string[len(users)-1], color = 0xDEADBF) # Join the array together into a string for the group touch (not orgy)
            fancy.set_image(url = chosen)
            await kizuna.say(embed = fancy)
        else: # Display a special message if the array is empty
            await kizuna.say("Enter a valid username you dumbass")
    else: # Display a special message of touchee has no arguements
        await kizuna.say("You touched no one dumbass")

async def ai(discord,kizuna):
    image = access.randomImage("ai")
    chosen = random.choice(image)

    fancy = discord.Embed()
    fancy.set_image(url = chosen)
    await kizuna.say(embed = fancy)
