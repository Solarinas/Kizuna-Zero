#JSON for future use
import json

#database class for fun module
class fun(object):

    #starting properties for "fun" database object
    def __init__(self, commands):
        for i in commands:
            setattr(self, i + "DB", dict())
        self.commands = commands
        #manager that will handle database for purposes outside of the app
        self.manager = dbManager(self)
        self.manager.loadJSON()
        '''
        It's the same as doing:
        self.hugDB = dict()
        self.touchDB = dict()
        '''

    #function to update counter for user
    def updateCounter(self, command, userID, targetIDs):
        #if user exists
        if self.manager.entryExists(self, command, userID):
            #grab database
            data = getattr(self, command + "DB")
            #check if counter data is "None"
            if data[userID]["counter"]:
                #grab user based on id then their "counter" dict and add 1 to each user they hugged
                for i in targetIDs:
                    if i in data[userID]["counter"]:
                        data[userID]["counter"][i] += 1
                    else:
                        data[userID]["counter"][i] = 1
            else:
                data[userID]["counter"] = dict()
                for i in targetIDs:
                    data[userID]["counter"][i] = 1
            #success return True
            self.manager.saveJSON()
            return True
        #user does not exist in db return False
        return False

    #function to get user counter
    def getUserCount(self, command, userID, targetIDs):
        #if user exists
        if self.manager.entryExists(self, command, userID):
            #create return dict
            retCounter = dict()
            #grab database
            data = getattr(self, command + "DB")
            if data[userID]["counter"]:
                #success return amount
                for i in targetIDs:
                    retCounter["<@"+i+">"] = data[userID]["counter"][i]
                return retCounter
            #when the user has "None"
            else:
                return dict()
        #user does not exist in db return False
        return False

class dbManager:

    def __init__(self, db):
        self.db = db

    def saveJSON(self):
        for i in self.db.commands:
            with open("db/" + i + ".json", "w+") as f:
                f.write(json.dumps(getattr(self.db, i + "DB")))

    def loadJSON(self):
        try:
            try:
                for i in self.db.commands:
                    with open("db/" + i + ".json", "r") as f:
                        setattr(self.db, i + "DB", json.load(f))
            except FileNotFoundError:
                self.createJSON()
        #if file is empty
        except TypeError:
            pass

    def createJSON(self):
        for i in self.db.commands:
            with open("db/" + i + ".json", "w+") as f:
                f.write(json.dumps(getattr(self.db, i + "DB")))

#function to check if user exists in database
    def entryExists(self, module, command, id):
        #user get attribute to grab right db based on properties defined
        data = getattr(module, command + "DB")
        #if user id is a key in that specific db return True
        if id in data:
            return True
        return False #else no results


    #function to add user to database
    def addEntry(self, module, command, id):
        #use previous function to check if user doesn't exists
        if not self.entryExists(module, command, id):
            #user get attribute to grab right db based on properties defined
            data = getattr(module, command + "DB")
            #new entry for an id key will be another dictionary
            data[id] = dict()
            #dictionary within dictionary's key will be based on module command dictionary
            for x in module.commands[command]:
                data[id][x] = None
            self.saveJSON()
            return True
        #if user exists already return False
        return False

    def returnAttribute(self, module, command, attribute, id):
        data = getattr(module, command + "DB")
        return data[id][attribute]

class events(object):
    def __init__(self, commands):
        for i in commands:
            setattr(self, i + "DB", dict())
        self.commands = commands
        self.manager = dbManager(self)
        self.manager.loadJSON()

    def updateWelcome(self, command, channel, msg, id):
        if self.manager.entryExists(self, command, id):
            data = getattr(self, command + "DB")
            data[id]["channelID"] = channel
            data[id]["msg"] = " ".join(msg)
            self.manager.saveJSON()
            return True
        return False

class admin(object):
  def __init__(self, commands):
        for i in commands:
            setattr(self, i + "DB", dict())
        self.commands = commands
        self.manager = dbManager(self)
        self.manager.loadJSON()

  def disable(self, module, command, attribute, id):
        if self.manager.entryExists(self, command, id):
            data = getattr(self, command + "DB")
            data[id][attribute] = False
            self.manager.saveJSON()
            return True
        return False

  def enable(self, module, command, attribute, id):
        if self.manager.entryExists(self, command, id):
            data = getattr(self, command + "DB")
            data[id][attribute] = True
            self.manager.saveJSON()
            return True
        return False
