def getUser(discord, db, arg, ctx):
    '''
    Input:
    - discord (discord): pass discord object from app
    - db      (list)   : pass ctx.message.channel.members
    - arg     (str)    : string id or username to search for

    Output:
    - returns member/User object if user is found
    - returns False if there is no match
    '''
    #make an array of searches to prevent so many nested if and else statements
    searches = [
        discord.utils.get(db, id = arg),
        discord.utils.find(lambda m: arg.lower() in m.name.lower() and ctx.message.channel.permissions_for(m).read_messages, db),
        discord.utils.get(db, id = cleanPingString(arg))
        ]
    for i in searches:
        if i:
            return i
    return False

def getUsers(discord, db, args, ctx):
    '''
    Input:
    - discord (discord): pass discord object from app
    - db      (list)   : pass ctx.message.channel.members
    - args    (list)   : bunch of strings to search against

    Output:
    - returns list of mentionable users
    - if member not found, they are not added to output list
      Example: ["12rmaker", "Solarinas"]
    '''
    #create empty user list
    users = []
    #loop through search string list "args"
    for i in args:
        #user previous "getUser" function on each element of args
        member = getUser(discord, db, i, ctx)
        #check if we got actual results
        if member:
            #then add to user list
            users.append(member)
    return users

def channelExists(discord, arg, ctx):
    channel = discord.utils.get(ctx.message.server.channels, name = arg)
    if channel:
        return True
    return False

def duplicateCleaner(items):
    newList = []    # Create a new array for a new list
    for i in items:
        if i not in newList: # if item is not in the new list then add it into the new list
            newList.append(i)
    return newList

def cleanPingString(arg):
    #ping check using raw string
    if "<@" in arg and ">" in arg and "<@!" not in arg:
        #split <@ out of string
        ident = arg.split("<@")[1]
        #split out > at end now
        ident = ident.split(">")[0]
        return ident
    elif "<@!" in arg and ">" in arg:
        #split <@! out of string
        ident = arg.split("<@!")[1]
        #split out > at end now
        ident = ident.split(">")[0]
        return ident

def time_to_string(total_seconds):
    m = 60
    h = m * 60
    d = h * 24
    # Get the days, hours, etc:
    days = int(total_seconds / d )
    hours = int((total_seconds % d) / h)
    minutes = int((total_seconds % h) / m)
    seconds = int(total_seconds % m)
    res = list()
    if days > 0:
        res.append(days == 1 and "**1** day" or "**%s**" % (days) + " days")
    if hours > 0:
        res.append(hours == 1 and "**1** hour" or "**%s**" % (hours) + " hours" )
    if minutes > 0:
        res.append(minutes == 1 and "**1** minute" or "**%s**" % (minutes) + " minutes")
    if seconds > 0:
        res.append(seconds == 1 and "**1** second" or "**%s**" % (seconds) + " seconds")
    else:
     if len(res) == 0:
         res.append("**0** second")
    return ", ".join(res)
