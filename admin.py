import database
import asyncio

commands = {"disable":{"welcome": [bool]}}
admin_db = database.admin(commands)

async def disable(discord, kizuna, ctx, command):
    if ctx.message.author.server_permissions.administrator:
        if command in commands["disable"]:
            if admin_db.manager.entryExists(admin_db, "disable", ctx.message.server.id):
                admin_db.disable(admin_db, "disable", command, ctx.message.server.id)
            else:
                admin_db.manager.addEntry(admin_db, "disable", ctx.message.server.id)
                admin_db.disable(admin_db, "disable", command, ctx.message.server.id)
            await kizuna.say(command + " command is disabled")
        else:
            await kizuna.say("Please enter a valid command to disable!")
    else:
        await kizuna.say("You must be an admin to use this command")

async def enable(discord, kizuna, ctx, command):
    if ctx.message.author.server_permissions.administrator:
        if command in commands["disable"]:
            if admin_db.manager.entryExists(admin_db, "disable", ctx.message.server.id):
                admin_db.enable(admin_db, "disable", command, ctx.message.server.id)
            else:
                admin_db.manager.addEntry(admin_db, "disable", ctx.message.server.id)
                admin_db.enable(admin_db, "disable", command, ctx.message.server.id)
            await kizuna.say(command + " command is enabled")
        else:
            await kizuna.say("Please enter a valid command to enable!")
    else:
        await kizuna.say("You must be an admin to use this command")
